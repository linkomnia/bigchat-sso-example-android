package co.bigcoin.bigchat.ssoexample;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final int BIGCHAT_LOGIN_REQUEST_CODE = 0x1A4C618;

    private TextView mTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTextView = (TextView) findViewById(R.id.textView);

        final Button loginButton = (Button) findViewById(R.id.loginButton);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startBigChatLogin(view);
            }
        });
    }

    private void startBigChatLogin(View view) {
        mTextView.setText("Logging in...");

        final Intent intent = new Intent("co.bigchat.intent.SSO_LOGIN");

        // make sure BigChat is installed
        final PackageManager pm = getPackageManager();
        final List activities = pm.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
        if (activities.size() == 0) {
            // BigChat is not installed
            mTextView.setText("BigChat is not installed!");
            return;
        }

        final Bundle opts = ActivityOptionsCompat.makeScaleUpAnimation(view, 0, 0, view.getWidth(), view.getHeight()).toBundle();
        ActivityCompat.startActivityForResult(MainActivity.this, intent, BIGCHAT_LOGIN_REQUEST_CODE, opts);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case BIGCHAT_LOGIN_REQUEST_CODE:
                onBigChatLoginResult(resultCode, data);
                break;
        }
    }

    private void onBigChatLoginResult(int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_CANCELED) {
            mTextView.setText("Login cancelled");
        } else if (resultCode == Activity.RESULT_OK) {
            String authToken = data.getStringExtra("co.bigchat.sso.AuthToken");
            mTextView.setText("Login successful, token = " + authToken);
        }
    }
}
